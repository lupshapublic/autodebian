#!/bin/bash

# once you have the DEBIAN Unattended install ISO created, you can use this script to provision your VMs.

if [ "$#" -ne 2 ]; then
    echo "Usage: $0 <vm-name> <mac ex 52:54:00:??:??:??> (the mac MUST begin with 52:54:00)"
    echo "ex: $0 worker1 52:54:00:11:22:01"
    exit 1
fi

VMNAME="$1"
MAC="$2"

# get the ISO and place it in your custom location, such as in /tmp
# ex. curl -o /tmp/UNATTENDED-debian-12.4.0-amd64-DVD-1.iso http://piweb.florida/ISOs/UNATTENDED-debian-12.4.0-amd64-DVD-1.iso
UNATTENDED=/tmp/UNATTENDED-debian-12.4.0-amd64-DVD-1.iso

if [ -e "$UNATTENDED" ]; then
    echo "unattended ISO found at: $UNATTENDED"
else
    echo "unattended ISO missing, please place it at: $UNATTENDED"
    echo "ex: curl -o /tmp/UNATTENDED-debian-12.4.0-amd64-DVD-1.iso http://piweb.florida/ISOs/UNATTENDED-debian-12.4.0-amd64-DVD-1.iso"
    exit 1
fi

sudo virt-install \
  --name=$VMNAME \
  --ram=2048 \
  --vcpus=2 \
  --disk path=/var/lib/libvirt/images/$VMNAME.qcow2,size=20 \
  --os-variant debian12 \
  --network bridge=br0,mac=$MAC \
  --graphics none \
  --console pty,target_type=serial \
  --location '/tmp/UNATTENDED-debian-12.4.0-amd64-DVD-1.iso' \
  --extra-args 'console=ttyS0,115200n8 serial'

