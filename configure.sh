#!/bin/bash

# feel free to change this section
DEFAULT_ISO_URL=https://cdimage.debian.org/debian-cd/current/amd64/iso-dvd/debian-12.4.0-amd64-DVD-1.iso
#DEFAULT_ISO_URL=http://piweb.florida/ISOs/debian-12.4.0-amd64-DVD-1.iso
#DEFAULT_ISO_URL=http://piweb.florida/ISOs/debian-12.4.0-amd64-netinst.iso

DEFAULT_SERVERNAME=autodebian
DEFAULT_USER=debian
DEFAULT_PASSWORD=debian
# end of customizable section

read -p "Ubuntu ISO URL [Default: $DEFAULT_ISO_URL]: " ISO_URL
if [ -z "$ISO_URL" ]; then
  ISO_URL=$DEFAULT_ISO_URL
fi
read -p "New server name [Default: $DEFAULT_SERVERNAME]: " SERVERNAME
if [ -z "$SERVERNAME" ]; then
  SERVERNAME=$DEFAULT_SERVERNAME
fi
read -p "User to create [Default: $DEFAULT_USER]: " USER
if [ -z "$USER" ]; then
  USER=$DEFAULT_USER
fi
read -p "Password to set for user $USER [Default: $DEFAULT_PASSWORD]: " PASSWORD
if [ -z "$PASSWORD" ]; then
  PASSWORD=$DEFAULT_PASSWORD
fi

banner() {
    local text="$@"
    local length=${#text}
    echo "╔$(printf '═%.0s' $(seq 1 $length))╗"
    while IFS= read -r line; do
        printf "║%s║\n" "$line"
    done <<< "$text"
    echo "╚$(printf '═%.0s' $(seq 1 $length))╝"
}

SUMMARY="ISO_URL:$ISO_URL
SERVERNAME:$SERVERNAME
USER:$USER
PASSWORD:$PASSWORD"
banner $SUMMARY

banner "installing necessary utils"
sudo apt install -y xorriso wget whois

# create needed dirs
if [ -d "./isofiles" ]; then
  banner "purging old ./isofiles dir"
  chmod u+w -R ./isofiles
  rm -rf "./isofiles"
fi
mkdir -p ./isofiles/

# get a copy of the original live server iso and place it in the home ISOs directory
mkdir -p ./ISOs
ISOFILENAME=$(basename $ISO_URL)
if [ -e "./ISOs/${ISOFILENAME}" ]; then
  banner "ISO already downloaded: ./ISOs/${ISOFILENAME}"
  ls -la ./ISOs/${ISOFILENAME}
else
  banner "ISO not present, downloading $ISO_URL"
  wget -P ./ISOs/ $ISO_URL
fi


banner "extracting ISO"
xorriso -osirrox on -indev ./ISOs/${ISOFILENAME} -extract / isofiles/


banner "adding preseed to initrd"
cp ./preseed.cfg.template ./preseed.cfg
HASHEDPASSWORD=`mkpasswd --method=SHA-512 $PASSWORD`
sed -i "s/THESERVERNAME/${SERVERNAME}/g" ./preseed.cfg
sed -i "s/THEUSERNAME/${USER}/g" ./preseed.cfg
echo "d-i passwd/user-password-crypted password ${HASHEDPASSWORD}" >> ./preseed.cfg
echo "# done, good luck" >> ./preseed.cfg

chmod +w -R isofiles/install.amd/
gunzip isofiles/install.amd/initrd.gz
echo preseed.cfg | cpio -H newc -o -A -F isofiles/install.amd/initrd


banner "repackaging initrd"
gzip isofiles/install.amd/initrd
chmod -w -R isofiles/install.amd/
ls -la isofiles/install.amd/initrd.gz


banner "editing isolinux.cfg to avoid install menu"
chmod u+w ./isofiles/isolinux/
chmod u+w ./isofiles/isolinux/isolinux.cfg
sed -i '/vesamenu.c32/s/^/#/' ./isofiles/isolinux/isolinux.cfg
chmod -w ./isofiles/isolinux/isolinux.cfg
chmod -w ./isofiles/isolinux/
ls -la ./isofiles/isolinux/isolinux.cfg


banner "editing grub to avoid menu"
GRUB=./isofiles/boot/grub/grub.cfg
chmod u+w $GRUB
echo -e "set timeout_style=hidden" >> $GRUB
echo -e "set timeout=0" >> $GRUB
echo -e "set default=1" >> $GRUB
chmod -w $GRUB

banner "generating MD5 sum"
cd isofiles/
chmod a+w md5sum.txt
md5sum `find -follow -type f` > md5sum.txt
chmod a-w md5sum.txt
cd ..

banner "creating final ISO"
chmod a+w isofiles/isolinux/isolinux.bin
genisoimage -r -J -b isolinux/isolinux.bin -c isolinux/boot.cat -no-emul-boot -boot-load-size 4 -boot-info-table -o UNATTENDED-${ISOFILENAME} isofiles

banner "done!"
ls -la UNATTENDED-${ISOFILENAME}

