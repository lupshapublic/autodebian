# autodebian

Build an automated unattended Debian self-install image. This means no prompts, no selections, no user setup, 
it's all done in 1 step. It's like Vagrant, but you have full control over your image.

# Build the image once

```
git clone https://gitlab.com/lupshapublic/autodebian.git
cd autodebian
chmod u+x ./configure.sh
```

and run:
```
./configure.sh
```

Example resulting .iso: ```UNATTENDED-debian-12.4.0-amd64-DVD-1.iso``` That's the unattended ISO to use from now on.


# Provision a VM (method 1)

Use the provision.sh script

```
./provision.sh lr3-controlplane 52:54:00:01:20:04
```

and let it run.

![installing for a couple minutes](image-6.png)

![finishing install](image-7.png)

![logged in](image-8.png)

# Provision a VM (method 2, the long way)

Here you start virt-manager and click around and select things, to set up your VM.

![create a new VM](image.png)

You have to uncheck "Automatically detect" and type in "Debian" and select Debian 12.

![default 2 gigs of RAM and 2 CPUs](image-1.png)

![assign it the default 20 gig drive](image-2.png)

![name it test20](image-3.png)

![installing...](image-4.png)

![KVM display](image-5.png)

Example unattended install in KVM/VirtManager
(more info at: https://help.ubuntu.com/community/KVM/VirtManager )


